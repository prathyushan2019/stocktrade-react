import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import Table from './components/stock_table_data';
import 'bootstrap/dist/css/bootstrap.min.css';


class App extends Component {
  state = {
    stockdata: []
  }
 
 
  componentDidMount() {
    try {
      setInterval(async () => {
        const res = await fetch('http://stocktrade-env.eba-mc3yyfgn.ap-southeast-1.elasticbeanstalk.com/getAllStocks');
        const data = await res.json();
        this.setState({
          stockdata: data
        })
      }, 500);
    } catch(e) {
      console.log(e);
    }
  }

  render() {
    return (
      <div className="App">
      <Table stockdata={ this.state.stockdata } />
    </div>
    )
  }

}
 
export default App;
