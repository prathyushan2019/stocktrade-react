import React from 'react';
import { useState, useEffect, useRef } from 'react';
import { ArrowUp, ArrowDown } from 'react-bootstrap-icons';

const Table = ({ stockdata }) => {
    if(stockdata !== undefined && stockdata.length>0)
    {
    return (
      
      <table className="table">
        <thead>
          <tr>
            <th>Symbol</th>
            <th>Price</th>
            <th>Trend</th>
          </tr>
        </thead>
        <tbody>
        { (stockdata.length > 0) ? stockdata.map( (stockdata, index) => {
           return (
            <tr key={ index }>
              <td>{ stockdata.symbol }</td>
              <td>{ marketPrice(stockdata) }</td>
              <td> { (stockdata.bidPrice < stockdata.askPrice) ? <ArrowUp fill="green" />:<ArrowDown fill="red" /> } </td>
        
            </tr>
          )
         }) : <tr><td colSpan="3">Loading...</td></tr> }
        </tbody>
      </table>
    );
    }
    else
    {
      return ( <dl> <h4> Problem in loading the Data </h4>
                         <dt> </dt>
                         <dd> * Please check the Network </dd>
                         <dd> * Check the Setup </dd> </dl> );
    }

    function marketPrice(stockdata){
        return (stockdata.bidPrice +stockdata.askPrice) / 2
    }

        
  function GetTrend(stockdata){
    const [price, setPrice] = useState(marketPrice(stockdata));
    const prevPrice = usePrevious(price)
    debugger
    if(prevPrice==price){
        return "eq"
    }else if(prevPrice>price) {
        return "up"
    }else if(prevPrice<price) {
        return "down"
    }
  }

  function usePrevious(value) {
    const ref = useRef();
    useEffect(() => {
      ref.current = value;
    });
    return ref.current;
  }
    
  }





  export default Table;